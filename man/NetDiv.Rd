\name{Netdiv-package}
\alias{NetDiv-package}
\alias{NetDiv}
\docType{package}
\title{
Diversity indices and dissimilarities for networks
}
\description{
Computation of diversity indices and dissimilarities for networks.
}
\details{
This package allow to compute and partitionate diversity indices for networks while aggregating the nodes and links into higher relevant entities.
}
\author{
Authors: Stéphane Dray, Vincent Miele, Marc Ohlmann, Wilfried Thuiller
  
Maintainer: Marc Ohlmann <marc.ohlmann@univ-grenoble-alpes.fr>
}
\references{

}
\keyword{ package }
\keyword{ ecology }
\keyword{ diversity }
\keyword{ network }
%\seealso{
%Optional links to other man pages, e.g. \code{\link[<pkg>:<pkg>-package]{<pkg>}}
%}
%\examples{
%}
